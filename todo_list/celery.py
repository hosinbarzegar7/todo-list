from __future__ import absolute_import
import os
from celery import Celery
from django.conf import settings

from todo_list.local_settings import CUSTOME_BROKER_URL

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'todo_list.settings')
app = Celery('todo_list', broker=CUSTOME_BROKER_URL)


app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
