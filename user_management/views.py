from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import CreateView

from user_management.forms import UserSignUp


class LogOutView(LogoutView):
    next_page = '/auth/login/'


class LoginUser(LoginView):
    template_name = "user_management/user_login.html"
    next_page = reverse_lazy("create-task")


class SignupUser(CreateView):
    form_class = UserSignUp
    template_name = "user_management/sign_up.html"
    success_url = reverse_lazy("login-user")
