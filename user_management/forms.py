from django import forms
from django.contrib.auth.hashers import make_password
from django.core.exceptions import ValidationError
from user_management.models import User


class UserSignUp(forms.ModelForm):
    confirm_password = forms.CharField(widget=forms.PasswordInput())
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = (
            "first_name",
            "last_name",
            "username",
            "password",
            "confirm_password",
            "phone_number",
        )

    def clean_phone_number(self):
        phone_number = self.cleaned_data["phone_number"]
        if len(phone_number) != 11:
            raise ValidationError("length is to 11")
        return phone_number

    def clean_confirm_password(self):
        cleaned_data = super().clean()
        password = cleaned_data['password']
        confirm_password = cleaned_data["confirm_password"]
        if password != confirm_password:
            raise ValidationError("Passwords does not match")
        cleaned_data['password'] = make_password(cleaned_data['password'])
        return cleaned_data
