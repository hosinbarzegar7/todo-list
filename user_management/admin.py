from django.contrib import admin
from django.contrib.admin import register

from user_management.models import User


@register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', "first_name", "last_name", "phone_number", "is_active"]
    list_filter = ["is_active"]
    list_editable = ["is_active"]
    search_fields = ["last_name", "phone_number"]
