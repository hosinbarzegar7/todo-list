from django.urls import path

from user_management.views import LoginUser, SignupUser, LogOutView

urlpatterns = [
    path("login/", LoginUser.as_view(), name="login-user"),
    path("logout/", LogOutView.as_view(), name="logout-user"),
    path("signup/", SignupUser.as_view(), name="signup-user"),
]
