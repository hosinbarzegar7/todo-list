from celery import shared_task

from task_management.models import Task
from utility.email_notifier import EmailNotifier
from utility.sms_notifier import SMSNotifier


@shared_task
def send_sms_notify(task_id, message):
    task = Task.objects.get(id=task_id)

    sms_notifier = SMSNotifier('hosein', '09134474495', '1234')
    try:
        sms_notifier.verify()
    except Exception as e:
        print(f'got error while try to verify mail server and sendsms service, error: {e}')
        return

    for member in task.members.all():
        sms_notifier.send_notification(member.phone_number, message)


@shared_task
def send_email_notify(task_id, message):
    task = Task.objects.get(id=task_id)

    email_notifier = EmailNotifier("xxxx", 9090, 'hosein', '1234')
    try:
        email_notifier.verify()
    except Exception as e:
        print(f'got error while try to verify mail server and sendsms service, error: {e}')
        return

    for member in task.members.all():
        email_notifier.send_notification(f'{member.username}@gmail.com', message)
