from task_management.models import Task
from django import forms


class TaskCreateForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ('title', 'description', 'members', 'files')


class UpdateTaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ('title', 'description', 'members', 'status')


class DeleteTaskForm(forms.ModelForm):

    class Meta:
        model = Task
        fields = ('title', 'description', 'members', 'status')
