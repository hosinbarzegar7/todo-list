from django.db.models.signals import m2m_changed
from django.dispatch import receiver

from task_management.models import Task
from task_management.tasks import send_email_notify, send_sms_notify


@receiver(m2m_changed, sender=Task.members.through)
def sms_notify(sender, instance: Task, action, **kwargs):
    if action == 'post_add':
        for member in instance.members.all():
            send_sms_notify.delay(instance.id, f'SMS member: {member} assigned to task: {instance}')
            send_email_notify.delay(instance.id, f'EMAIL member: {member} assigned to task: {instance}')
