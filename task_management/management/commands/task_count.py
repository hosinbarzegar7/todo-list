import json
from django.core.management import BaseCommand
from django.core.exceptions import FieldError

from task_management.models import Task


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('--status')
        parser.add_argument('--filters')

    def handle(self, *args, **options):
        base_query = Task.objects.all()
        status = options['status']
        if status:
            if int(status) not in [item[0] for item in Task.status.field.choices]:
                print(f'status {status} is not a valid choice')
                return
            base_query = base_query.filter(status=status)

        filters = options['filters']
        _filters = None
        if filters:
            try:
                _filters = json.loads(filters)
            except json.decoder.JSONDecodeError:
                print('filters is not valid')
                return
        if _filters:
            try:
                base_query = base_query.filter(**_filters)
            except FieldError as e:
                print(f"filter queryset by params: {_filters} got error: {e}")
                return

        print(base_query.count())
