from django.urls import path

from task_management.views import ListUserTask, CreateTask, UpdateTask, DeleteTask

urlpatterns = [
    path("list/", ListUserTask.as_view(), name="list-task"),
    path("create/", CreateTask.as_view(), name="create-task"),
    path("update/<int:pk>/", UpdateTask.as_view(), name="update-task"),
    path("delete/<int:pk>/", DeleteTask.as_view(), name="delete-task"),
]
