from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from file_management.models import File
from .forms import TaskCreateForm, UpdateTaskForm
from .models import Task


class ListUserTask(LoginRequiredMixin, ListView):
    model = Task
    template_name = "task_management/list_tasks.html"

    def get_queryset(self):
        return super().get_queryset().filter(
            creator=self.request.user
        )


class CreateTask(LoginRequiredMixin, CreateView):
    form_class = TaskCreateForm
    template_name = "task_management/create_task.html"
    success_url = reverse_lazy("list-task")

    def get_form(self, *args, **kwargs):
        form = super().get_form(None)
        form.fields["files"].queryset = File.objects.filter(uploader=self.request.user)
        return form

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.creator = self.request.user
        instance.save()
        return super().form_valid(form)

    def get_queryset(self):
        super().get_queryset.filter(
            creator=self.request.user
            )


class UpdateTask(LoginRequiredMixin, UpdateView):
    model = Task
    form_class = UpdateTaskForm
    template_name = 'task_management/update_task.html'
    success_url = reverse_lazy('list-task')

    def get_queryset(self):
        return super().get_queryset().filter(
            creator=self.request.user
        )


class DeleteTask(LoginRequiredMixin, DeleteView):
    model = Task
    template_name = 'task_management/delete_task.html'
    success_url = reverse_lazy('list-task')

    def get_queryset(self):
        return super().get_queryset().filter(
            creator=self.request.user
        )
