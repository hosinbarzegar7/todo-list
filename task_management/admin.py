from django.contrib import admin

from django.contrib.admin import register
from file_management.models import File
from task_management.models import Task


class FilesInline(admin.TabularInline):
    model = File


@register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'creator', "description", "status")
    list_select_related = ('creator',)
    list_filter = ("status",)
    list_editable = ('status',)
    filter_horizontal = ('members', 'files')
    # inlines = (FilesInline,)
