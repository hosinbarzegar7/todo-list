from django.db import models

from file_management.models import File
from user_management.models import User


class Task(models.Model):
    CREATED = 1
    DOING = 2
    DONE = 3
    CANCEL = 4
    PAUSE = 5
    ATTRIBUTE_TYPES = (
        (CREATED, "task_create"),
        (DOING, "task_doing"),
        (DONE, "task_done"),
        (CANCEL, "task_cancel"),
        (PAUSE, "task_stop"),
    )

    created_time = models.DateTimeField(auto_now_add=True)
    modified_time = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=200)
    description = models.TextField()
    status = models.PositiveSmallIntegerField(default=CREATED, choices=ATTRIBUTE_TYPES)

    creator = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="tasks"
    )
    members = models.ManyToManyField(User, blank=True)
    files = models.ManyToManyField(File, blank=True)

    def __str__(self):
        return f"{self.title} - {self.status}"
