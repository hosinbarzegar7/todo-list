from abc import ABC, abstractmethod


class BaseAuthenticated(ABC):

    @abstractmethod
    def verify(self):
        pass

    @abstractmethod
    def send_notification(self, *args, **kwargs):
        pass
