from time import sleep

from .abstracts import BaseAuthenticated


class SMSNotifier(BaseAuthenticated):
    __username = 'hosein'
    __password = '1234'

    def __init__(self, username, sender_phone_number, password):
        self.username = username
        self.sender_phone_number = sender_phone_number
        self.password = password
        self.__is_authenticated = True

    def verify(self):
        if self.username != self.__username or self.password != self.__password:
            raise AssertionError("username or password is not correct")
        else:
            self.__is_authenticated = True

    def send_notification(self, receiver_phone_number, message):
        sleep(5)
        if not self.__is_authenticated:
            raise AssertionError('you are not verified')
        else:
            print(f'message: {message} sent to: {receiver_phone_number} successfully')
