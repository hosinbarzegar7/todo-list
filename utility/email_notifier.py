from time import sleep

from .abstracts import BaseAuthenticated


class EmailNotifier(BaseAuthenticated):
    __username = 'hosein'
    __password = '1234'

    def __init__(self, mail_server, port, username, password):
        self.mail_server = mail_server
        self.port = port
        self.username = username
        self.password = password
        self.__is_authenticated = False

    def verify(self):
        if self.username != self.__username or self.password != self.__password:
            raise AssertionError("username or password is not correct")
        else:
            self.__is_authenticated = True

    def send_notification(self, mail, message):
        sleep(5)
        if not self.__is_authenticated:
            raise AssertionError('you are not verified')
        else:
            print(f'message: {message} sent to: {mail} successfully')
