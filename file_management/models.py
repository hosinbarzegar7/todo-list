import uuid
from django.db import models

from user_management.models import User


class File(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    created_time = models.DateTimeField(auto_now_add=True)
    file = models.FileField(upload_to='task/')
    uploader = models.ForeignKey(User, related_name='files', on_delete=models.CASCADE)
