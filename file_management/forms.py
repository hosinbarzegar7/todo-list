from django import forms
from file_management.models import File


class FileForm(forms.ModelForm):

    class Meta:
        model = File
        fields = ('file',)

    def __init__(self, *args, **kwargs):
        super(FileForm, self).__init__(*args, **kwargs)
        self.fields['file'].required = False
