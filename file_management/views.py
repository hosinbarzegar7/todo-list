from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, DeleteView, ListView

from file_management.forms import FileForm
from file_management.models import File


class FileUpload(LoginRequiredMixin, CreateView):
    model = File
    form_class = FileForm
    template_name = 'file_management/upload_file.html'
    success_url = reverse_lazy('create-task')

    def form_valid(self, form):
        instance = form.save(commit=False)
        instance.uploader = self.request.user
        instance.save()
        return super().form_valid(form)


class ListFiles(LoginRequiredMixin, ListView):
    model = File
    template_name = "file_management/list_file.html"

    def get_queryset(self):
        return super().get_queryset().filter(
            uploader=self.request.user
        )


class DeleteFile(LoginRequiredMixin, DeleteView):
    model = File
    template_name = 'file_management/delete_file.html'
    success_url = reverse_lazy('list-file')

    def get_queryset(self):
        return super().get_queryset().filter(
            uploader=self.request.user
        )
