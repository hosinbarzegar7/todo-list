from django.urls import path

from file_management.views import FileUpload, ListFiles, DeleteFile

urlpatterns = [
    path('upload-file/', FileUpload.as_view(), name='upload-file'),
    path('list-file/', ListFiles.as_view(), name='list-file'),
    path('delete-file/<uuid:pk>/', DeleteFile.as_view(), name="delete-file"),
]
