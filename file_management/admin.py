from django.contrib import admin

# Register your models here.
from django.contrib.admin import register

from file_management.models import File


@register(File)
class FileAdmin(admin.ModelAdmin):
    list_display = ('uploader', 'file', 'created_time')
